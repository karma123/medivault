import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import logo from "../../public/logo.png";

function RegistrationChoice() {
  const [selectedUser, setSelectedUser] = useState(null);
  const navigate = useNavigate();

  const handleCardClick = (userChoice) => {
    setSelectedUser(userChoice);
  };

  const handleSignupClick = () => {
    if (selectedUser === 1) {
      navigate("/userRegistration");
    } else if (selectedUser === 2) {
      navigate("/patientRegistration");
    } else {
      alert("Please select a user type before signing up.");
    }
  };

  return (
    <div className="flex justify-center items-center flex-col">
      <nav className="w-full pt-12 pb-12 flex justify-between px-52 items-center bg-black text-white">
      <Link to="/">
        < img  src={logo} alt="Logo" />   
      </Link>
      </nav>

      <div className="form-container w-[50vw] xs:p-10 sm:p-20 xl:px-40 xl:pt-20 xl:pb-32">
        <p className="text-center text-4xl pb-12">Choose User Type</p>

        <div className="flex justify-center items-center gap-10">
          <div
            className={`card w-60 h-60 rounded-lg shadow-md flex flex-col justify-center items-center cursor-pointer hover:scale-110 transition-transform ${
              selectedUser === 1 ? "border-2 border-blue-500" : ""
            }`}
            onClick={() => handleCardClick(1)}
          >
            {/* <svg className="w-32 h-32" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
              {/* Replace this with your preferred SVG icon for User */}
              <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z" />
            {/* </svg>  */}
            <svg width="120" height="120" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M20 0C16.5 0 14.405 0.492308 13.0733 1.10615C12.4067 1.41385 11.9333 1.73692 11.615 2.06769C11.3964 2.31348 11.2204 2.58906 11.0933 2.88462C11.0411 3.00817 11.0063 3.13745 10.99 3.26923L10.1033 13.7985C10.0917 13.8631 10.115 13.9677 10.1033 14.0385L10 15.0969C9.99029 15.1926 9.99029 15.2889 10 15.3846C10 20.4615 14.5 24.6154 20 24.6154C25.5 24.6154 30 20.4615 30 15.3846V15.1446C30.0003 15.1287 30.0003 15.1128 30 15.0969L29.01 3.26923C28.9937 3.13745 28.9589 3.00817 28.9067 2.88462C28.9067 2.88462 28.705 2.39846 28.385 2.06769C28.0667 1.73538 27.5933 1.41231 26.9267 1.10615C25.595 0.490769 23.5 0 20 0ZM20 24.6154C9 24.6154 0 29.3938 0 33.7015V40H40V33.7015C40 29.6092 31.8633 25.1015 21.615 24.6631C21.0774 24.6303 20.5388 24.6144 20 24.6154ZM20 3.07692C23.1667 3.07692 24.7483 3.53846 25.4167 3.84615C25.64 3.94923 25.6667 3.98923 25.73 4.03846L26.4067 11.9231C24.8733 12.0092 22.7833 12.3077 20 12.3077C17.2167 12.3077 15.1283 12.0092 13.5933 11.9231L14.27 4.03846C14.3317 3.98923 14.36 3.94923 14.5833 3.84615C15.2517 3.53846 16.8333 3.07692 20 3.07692ZM18.645 4.61538V6.44308H16.6667V8.94308H18.6467V10.7692H21.3533V8.94154H23.3333V6.44308H21.3533V4.61538H18.645ZM15 28.0769L19.2717 36.9231H3.33333V33.7015C3.33333 32.5554 7.58333 29.1985 15 28.0769ZM25 28.0769C32.4167 29.2 36.6667 32.5554 36.6667 33.7015V36.9231H20.7283L25 28.0769Z" fill="black"/>
            </svg>

            <p className="text-xl mt-4">User</p>
          </div>

          <div
            className={`card w-60 h-60 rounded-lg shadow-md flex flex-col justify-center items-center cursor-pointer hover:scale-110 transition-transform ${
              selectedUser === 2 ? "border-2 border-blue-500" : ""
            }`}
            onClick={() => handleCardClick(2)}
          >
            <svg width="120" height="120" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M10.0001 8C10.0001 3.58 14.4751 0 20.0001 0C25.5251 0 30 3.58 30 8C30 12.42 25.5251 16 20.0001 16C14.4751 16 10.0001 12.42 10.0001 8ZM32.5 40H35C37.75 40 40 38.2 40 36V26.44C40 24.2 38.475 22.14 35.975 21.12C34.9 20.68 33.75 20.26 32.5 19.88V40ZM20.8501 30L27.5001 18.66C25.1751 18.24 22.6751 18 20.0001 18C13.6751 18 8.22513 19.4 4.02515 21.12C2.8053 21.6226 1.78341 22.3859 1.07291 23.325C0.362406 24.2641 -0.00891336 25.3423 0.000162475 26.44V40H5.85014C5.30014 39.1 5.00014 38.08 5.00014 37C5.00014 33.14 8.92513 30 13.7501 30H20.8501ZM15.0001 40L18.5251 34H13.7501C11.6751 34 10.0001 35.34 10.0001 37C10.0001 38.66 11.6751 40 13.7501 40H15.0001Z" fill="black"/>
            </svg>

            <p className="text-xl mt-4">Patient</p>
          </div>
        </div>

        <button 
          className="w-full bg-[#8000FF] h-14 rounded-md shadow-xl text-white text-xl hover:bg-[#5808a8] hover:transition-all hover:duration-500 mt-10"
          onClick={handleSignupClick}
        >
          Sign up
        </button>
      </div>
    </div>
  );
}

export default RegistrationChoice;