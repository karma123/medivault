import React, { useEffect, useState } from "react";
import axios from "axios";
import JSZip from "jszip";
import SideNav from '../components/pages/sideNav'

function UploadFile() {
  const [file, setFile] = useState("");
  const [fileUrl, setFileUrl] = useState("");
 
  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      // Create a new JSZip instance
      const zip = new JSZip();

      // Add the uploaded file to the zip archive
      zip.file(file.name, file);

      // Convert form data to a JSON string
      const formData = {
        docName: e.target.docName.value,
        dateOfCreation: e.target.dateOfCreation.value,
      };
      const formDataJson = JSON.stringify(formData);

      // Add the form data as a JSON file to the zip archive
      zip.file("formData.json", formDataJson);

      // Generate the zip file
      const zipBlob = await zip.generateAsync({ type: "blob" });

      // Create FormData object to send the zip file
      const formDataForUpload = new FormData();
      formDataForUpload.append("file", zipBlob, "form_data.zip");

      // Make the API request to Pinata to upload the zip file
      const responseData = await axios({
        method: "post",
        url: "https://api.pinata.cloud/pinning/pinFileToIPFS",
        data: formDataForUpload,
        headers: {
          pinata_api_key: import.meta.env.VITE_PINATA_API_KEY,
          pinata_secret_api_key: import.meta.env.VITE_PINATA_SECRET_KEY,
          "Content-Type": `multipart/form-data`,
        },
      });

      // Set the file URL
      const fileUrl =
        "https://gateway.pinata.cloud/ipfs/" + responseData.data.IpfsHash;

      // // Store the IPFS hash in the browser's localStorage
      localStorage.setItem("ipfsHash", responseData.data.IpfsHash);

      setFileUrl(fileUrl);
    } catch (err) {
      console.error(err);
    }
  };

  return (
    <div className="bg-white pl-96 min-h-screen w-full text-black p-0 m-0">
      {/* <SideNav /> */}
      <h1>Upload the files</h1>
      <form onSubmit={handleSubmit}>
        <input type="text" name="docName" placeholder="Document Name" />
        <input
          type="date"
          name="dateOfCreation"
          placeholder="DateOfCreation (YYYY-MM-DD)"
        />
        <input type="file" onChange={(e) => setFile(e.target.files[0])} />
        <button type="submit">Upload</button>
      </form>
      {fileUrl && (
        <a href={fileUrl} target="_blank" rel="noopener noreferrer">
          {fileUrl}
        </a>
      )}
    </div>
  );
}

export default UploadFile;