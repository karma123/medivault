import SideNav from '../components/pages/sideNav'
import { useState } from 'react'
import Modal from "react-modal"
import kafka from "../assets/images/kafka.png"
import hot from "../assets/images/hot.png"
import document from "../assets/images/document.png"

Modal.setAppElement("#root")

const ProfileSvg = () => {
    return (
        <svg width="50px" height="50px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
    <g id="Dribbble-Light-Preview" transform="translate(-140.000000, -2159.000000)" fill="#000000">
        <g id="icons" transform="translate(56.000000, 160.000000)">
            <path d="M100.562548,2016.99998 L87.4381713,2016.99998 C86.7317804,2016.99998 86.2101535,2016.30298 86.4765813,2015.66198 C87.7127655,2012.69798 90.6169306,2010.99998 93.9998492,2010.99998 C97.3837885,2010.99998 100.287954,2012.69798 101.524138,2015.66198 C101.790566,2016.30298 101.268939,2016.99998 100.562548,2016.99998 M89.9166645,2004.99998 C89.9166645,2002.79398 91.7489936,2000.99998 93.9998492,2000.99998 C96.2517256,2000.99998 98.0830339,2002.79398 98.0830339,2004.99998 C98.0830339,2007.20598 96.2517256,2008.99998 93.9998492,2008.99998 C91.7489936,2008.99998 89.9166645,2007.20598 89.9166645,2004.99998 M103.955674,2016.63598 C103.213556,2013.27698 100.892265,2010.79798 97.837022,2009.67298 C99.4560048,2008.39598 100.400241,2006.33098 100.053171,2004.06998 C99.6509769,2001.44698 97.4235996,1999.34798 94.7348224,1999.04198 C91.0232075,1998.61898 87.8750721,2001.44898 87.8750721,2004.99998 C87.8750721,2006.88998 88.7692896,2008.57398 90.1636971,2009.67298 C87.1074334,2010.79798 84.7871636,2013.27698 84.044024,2016.63598 C83.7745338,2017.85698 84.7789973,2018.99998 86.0539717,2018.99998 L101.945727,2018.99998 C103.221722,2018.99998 104.226185,2017.85698 103.955674,2016.63598" id="profile_round-[#1342]">

</path>
        </g>
    </g>
</g>
</svg>
    )
}

const PatientCard = ({name, age, disease, handleOpenModal}) => {
   return (
    <div className='flex flex-col justify-center items-center bg-black p-12 rounded-xl' onClick={handleOpenModal}>
        <div className='bg-blue-500 rounded-full p-3'>
        <ProfileSvg/>
        </div>
        <div className='flex flex-col justify-center items-center pt-4'>
        <p className='text-2xl text-white'>{name}</p>
        <p className='text-xl text-white'>{age}</p>
        <p className='text-xl text-white'>{disease}</p>
        </div>  
    </div>
   )
}

const PatientData = [
    {
        name: 'John Doe',
        age: 45,
        disease: 'Diabetes',
        files: [
            { name: 'File1', type: 'jpg', uploadDate: '2022-01-01', image: kafka },
            { name: 'File2', type: 'jpg', uploadDate: '2022-01-02', image: hot },
        ]

    },
    {
        name: 'Jane Doe',
        age: 35,
        disease: 'Hypertension',
        files: [
            { name: 'File1', type: 'jpg', uploadDate: '2022-01-01', image: hot },
            { name: 'File2', type: 'jpg', uploadDate: '2022-01-02', image: kafka },
        ]

    },
    {
        name: 'James Doe',
        age: 55,
        disease: 'Cancer',
        files: [
            { name: 'File1', type: 'jpg', uploadDate: '2022-01-01', image: kafka },
            { name: 'File2', type: 'jpg', uploadDate: '2022-01-02', image: hot },
        ]

    },
    {
        name: 'Jill Doe',
        age: 65,
        disease: 'Arthritis',
        files: [
            { name: 'File1', type: 'jpg', uploadDate: '2022-01-01', image: hot },
            { name: 'File2', type: 'jpg', uploadDate: '2022-01-02', image: kafka },
        ]

    },
    {
        name: 'Jack Doe',
        age: 75,
        disease: 'Asthma',
        files: [
            { name: 'File1', type: 'jpg', uploadDate: '2022-01-01', image: hot },
            { name: 'File2', type: 'jpg', uploadDate: '2022-01-02', image: kafka },
        ]

    },
    {
        name: 'Jenny Doe',
        age: 85,
        disease: 'Bronchitis',
        files: [
            { name: 'File1', type: 'jpg', uploadDate: '2022-01-01', image: kafka },
            { name: 'File2', type: 'jpg', uploadDate: '2022-01-02', image: hot },
        ]

    }
]

function PatientPage() {
    const [searchTerm, setSearchTerm] = useState('');
    const [modalIsOpen, setModalIsOpen] = useState(false)
    const [selectedPatient, setSelectedPatient] = useState(null)
    const [showFile, setShowFile] = useState(false) 
    const [fileIndex, setFileIndex] = useState(null) // Add this line

    const [uploadModalIsOpen, setUploadModalIsOpen] = useState(false)

    const handleOpenUploadModal = () => {
        setUploadModalIsOpen(true);
    };

    const handleCloseUploadModal = () => {
        setUploadModalIsOpen(false);
    };

    const handleSearch = (event) => {
        setSearchTerm(event.target.value);
    };

    const handleOpenModal = (patient) => {
        setSelectedPatient(patient)
        setModalIsOpen(true);
    };

    const handleCloseModal = () => {
        setModalIsOpen(false);
    };

    const handleViewFile = (index) => {
        setShowFile(true)
        setFileIndex(index)
        
    }

    const handleFileUpload = (event) => {
        event.preventDefault();
        // handle the file upload here
    };


    const filteredPatients = PatientData.filter((patient) => {
        return patient.name.toLowerCase().includes(searchTerm.toLowerCase());
    });

    return (
        <div className='flex'>
            <SideNav />
            <div className='p-8 space-y-8'>
                <p className='text-4xl'>Your Patients</p>
                <input type="text" placeholder="Search by patient name" value={searchTerm} onChange={handleSearch}  className='border-2 border-black p-2 rounded-md '/>
                <div className="flex flex-wrap gap-20">
                    {
                        filteredPatients.map((patient) => {
                            return <PatientCard name={patient.name} age={patient.age} disease={patient.disease} handleOpenModal={() => handleOpenModal(patient)} /> // Pass the patient object to handleOpenModal
                        })
                    }
                </div>
            </div>
          <Modal
    isOpen={modalIsOpen}
    onRequestClose={handleCloseModal}
    contentLabel="Patient Details"
    style={{
        content: {
            width: '50%',
            margin: '0 auto', // This will center the modal
            padding: "2rem",
            background: "black"
        },
    }}
>
    <h2>Patient Files</h2>
    {selectedPatient && <p>{selectedPatient.name}</p>} {/* Display the selected patient's name */}
    {selectedPatient && selectedPatient.files && (
        <table className='w-full'>
            <thead className=' text-start'>
                <tr className='w-full'>
                    <th className='text-white text-start w-2/5'>File</th>
                    <th className='text-white text-start w-2/5'>Type</th>
                    <th className='text-white text-start w-1/5'>Upload Date</th>
                </tr>
            </thead>
            <tbody className='w-full'>
                {selectedPatient.files.map((file, index) => (
                    <tr key={index} className='w-full'>
                        <td className='text-white'>{file.name}</td>
                        <td className='text-white'>{file.type}</td>
                        <td className='text-white'>{file.uploadDate}</td>
                        <td className='text-white'><a href={file.image} download>Download</a></td>
                        <td className='text-white'><button onClick={() => handleViewFile(index)}>View</button></td>
                    </tr>
                ))}
            </tbody>
        </table>
    )}
    {
        showFile && <div className='w-full flex justify-center items-center pt-4'>
            <img src={selectedPatient.files[fileIndex].image
        } alt="file" onClick={() => setShowFile(false)}/>
        </div>

    }
    
    <div className='w-full justify-center items-center flex pt-12'>
    <button onClick={handleOpenUploadModal} className='text-white'>... Add Files... </button>
    </div>
    
    <button onClick={handleCloseModal} className='absolute top-2 right-2 text-white'>Close</button>
</Modal>
<Modal
                isOpen={uploadModalIsOpen}
                onRequestClose={handleCloseUploadModal}
                contentLabel="Upload Files"
                style={{
                    content: {
                        width: '50%',
                        margin: '0 auto', // This will center the modal
                        background: "black",
                        color: "white",
                        padding: "2rem",
                    },
                }}
            >
                <h2>Upload Files</h2>
                <form onSubmit={handleFileUpload} className='flex justify-center items-center flex-col'>
                    <img src={document} alt="" />
                    <div className='w-1/2 flex flex-col pt-8 space-y-4'>
                    <input type="text" placeholder='File name' className='rounded-md border border-white  p-2 text-white placeholder:text-white bg-black'/>
                    <input type="date" placeholder='File date' className='rounded-md border border-white  p-2 text-white placeholder:text-white bg-black'/>
                    <input type="text" placeholder='File type' className='rounded-md border border-white  p-2 text-white placeholder:text-white bg-black'/>
                    <input type="file" multiple  className='rounded-md border border-white  p-2 text-white placeholder:text-white bg-black'/>
                    </div>
                    <button type="submit" className='pt-8'>...Upload...</button>
                </form>
                <button onClick={handleCloseUploadModal}  className='absolute top-2 right-2'>Close</button>
            </Modal>
        </div>
    )
}

export default PatientPage;