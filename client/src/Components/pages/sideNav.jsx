import logo from "../../../public/logo.png";
import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";

const SideNav = () => {
  const [docName, setDocName] = useState("");
  const [docAge, setDocAge] = useState("");
  const [docAffiliation, setDocAffiliation] = useState("");
  const [docQualification, setDocQualification] = useState("");
  const [token, setToken] = useState("");

  const navigate = useNavigate()

  useEffect(() => {
    const storedToken = window.localStorage.getItem("token");
    setToken(storedToken);

    if (storedToken) {
      const fetchDoctorData = async () => {
        try {
          const response = await fetch(
            `https://medivault-backend-server.onrender.com/api/auth/getuserbytoken/${storedToken}`,
            {
              method: "GET",
              headers: {
                "Content-Type": "application/json",
              },
            }
          );

          const data = await response.json();
          if (data.status === "success") {
            console.log("dataSet", data)
            // console.log("name", data.doc.name)
            setDocName(data.doc.name);
            setDocAge(data.doc.age);
            setDocAffiliation(data.doc.affiliation);
            setDocQualification(data.doc.qualification);
          } else {
            console.error("Failed to fetch doctor data:", data.message);
          }
        } catch (error) {
          alert("An error occurred while fetching doctor data.");
          console.error("Fetch error:", error);
        }
      };

      fetchDoctorData();
    }
  }, []);

  const handleLogOut = () => { 
    window.localStorage.setItem("loggedIn", "false");
    window.localStorage.removeItem("token");
    navigate("/")
    
  }

  return (
    <div className="flex flex-col items-start justify-between p-4 text-white bg-black min-h-screen h-screen">
      <div>
        <img src={logo} alt="logo" />
        <div className="flex flex-row items-start">
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth={2}
            d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z"
          />
          <svg
            className="ml-10 mt-10"
            width="80"
            height="80"
            viewBox="0 0 40 40"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M20 0C16.5 0 14.405 0.492308 13.0733 1.10615C12.4067 1.41385 11.9333 1.73692 11.615 2.06769C11.3964 2.31348 11.2204 2.58906 11.0933 2.88462C11.0411 3.00817 11.0063 3.13745 10.99 3.26923L10.1033 13.7985C10.0917 13.8631 10.115 13.9677 10.1033 14.0385L10 15.0969C9.99029 15.1926 9.99029 15.2889 10 15.3846C10 20.4615 14.5 24.6154 20 24.6154C25.5 24.6154 30 20.4615 30 15.3846V15.1446C30.0003 15.1287 30.0003 15.1128 30 15.0969L29.01 3.26923C28.9937 3.13745 28.9589 3.00817 28.9067 2.88462C28.9067 2.88462 28.705 2.39846 28.385 2.06769C28.0667 1.73538 27.5933 1.41231 26.9267 1.10615C25.595 0.490769 23.5 0 20 0ZM20 24.6154C9 24.6154 0 29.3938 0 33.7015V40H40V33.7015C40 29.6092 31.8633 25.1015 21.615 24.6631C21.0774 24.6303 20.5388 24.6144 20 24.6154ZM20 3.07692C23.1667 3.07692 24.7483 3.53846 25.4167 3.84615C25.64 3.94923 25.6667 3.98923 25.73 4.03846L26.4067 11.9231C24.8733 12.0092 22.7833 12.3077 20 12.3077C17.2167 12.3077 15.1283 12.0092 13.5933 11.9231L14.27 4.03846C14.3317 3.98923 14.36 3.94923 14.5833 3.84615C15.2517 3.53846 16.8333 3.07692 20 3.07692ZM18.645 4.61538V6.44308H16.6667V8.94308H18.6467V10.7692H21.3533V8.94154H23.3333V6.44308H21.3533V4.61538H18.645ZM15 28.0769L19.2717 36.9231H3.33333V33.7015C3.33333 32.5554 7.58333 29.1985 15 28.0769ZM25 28.0769C32.4167 29.2 36.6667 32.5554 36.6667 33.7015V36.9231H20.7283L25 28.0769Z"
              fill="white "
            />
          </svg>
          <p className="ml-10 mt-16 text-2xl">{docName}</p>
        </div>
        <div className="flex flex-col items-start">
          <p className="ml-10 mt-16 text-2xl">Age: {docAge}</p>
          <p className="ml-10 mt-16 text-2xl">Afliliation:{docAffiliation}</p>
          <p className="ml-10 mt-16 text-2xl">
            Qualification: {docQualification}
          </p>
        </div>
      </div>
      <div className="flex flex-row items justify-end w-full">
        <svg
          width="40px"
          height="40px"
          viewBox="0 0 24 24"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <circle cx="12" cy="12" r="3" stroke="white" stroke-width="1.5" />
          <path
            d="M13.7654 2.15224C13.3978 2 12.9319 2 12 2C11.0681 2 10.6022 2 10.2346 2.15224C9.74457 2.35523 9.35522 2.74458 9.15223 3.23463C9.05957 3.45834 9.0233 3.7185 9.00911 4.09799C8.98826 4.65568 8.70226 5.17189 8.21894 5.45093C7.73564 5.72996 7.14559 5.71954 6.65219 5.45876C6.31645 5.2813 6.07301 5.18262 5.83294 5.15102C5.30704 5.08178 4.77518 5.22429 4.35436 5.5472C4.03874 5.78938 3.80577 6.1929 3.33983 6.99993C2.87389 7.80697 2.64092 8.21048 2.58899 8.60491C2.51976 9.1308 2.66227 9.66266 2.98518 10.0835C3.13256 10.2756 3.3397 10.437 3.66119 10.639C4.1338 10.936 4.43789 11.4419 4.43786 12C4.43783 12.5581 4.13375 13.0639 3.66118 13.3608C3.33965 13.5629 3.13248 13.7244 2.98508 13.9165C2.66217 14.3373 2.51966 14.8691 2.5889 15.395C2.64082 15.7894 2.87379 16.193 3.33973 17C3.80568 17.807 4.03865 18.2106 4.35426 18.4527C4.77508 18.7756 5.30694 18.9181 5.83284 18.8489C6.07289 18.8173 6.31632 18.7186 6.65204 18.5412C7.14547 18.2804 7.73556 18.27 8.2189 18.549C8.70224 18.8281 8.98826 19.3443 9.00911 19.9021C9.02331 20.2815 9.05957 20.5417 9.15223 20.7654C9.35522 21.2554 9.74457 21.6448 10.2346 21.8478C10.6022 22 11.0681 22 12 22C12.9319 22 13.3978 22 13.7654 21.8478C14.2554 21.6448 14.6448 21.2554 14.8477 20.7654C14.9404 20.5417 14.9767 20.2815 14.9909 19.902C15.0117 19.3443 15.2977 18.8281 15.781 18.549C16.2643 18.2699 16.8544 18.2804 17.3479 18.5412C17.6836 18.7186 17.927 18.8172 18.167 18.8488C18.6929 18.9181 19.2248 18.7756 19.6456 18.4527C19.9612 18.2105 20.1942 17.807 20.6601 16.9999C21.1261 16.1929 21.3591 15.7894 21.411 15.395C21.4802 14.8691 21.3377 14.3372 21.0148 13.9164C20.8674 13.7243 20.6602 13.5628 20.3387 13.3608C19.8662 13.0639 19.5621 12.558 19.5621 11.9999C19.5621 11.4418 19.8662 10.9361 20.3387 10.6392C20.6603 10.4371 20.8675 10.2757 21.0149 10.0835C21.3378 9.66273 21.4803 9.13087 21.4111 8.60497C21.3592 8.21055 21.1262 7.80703 20.6602 7C20.1943 6.19297 19.9613 5.78945 19.6457 5.54727C19.2249 5.22436 18.693 5.08185 18.1671 5.15109C17.9271 5.18269 17.6837 5.28136 17.3479 5.4588C16.8545 5.71959 16.2644 5.73002 15.7811 5.45096C15.2977 5.17191 15.0117 4.65566 14.9909 4.09794C14.9767 3.71848 14.9404 3.45833 14.8477 3.23463C14.6448 2.74458 14.2554 2.35523 13.7654 2.15224Z"
            stroke="white"
            stroke-width="1.5"
          />
        </svg>
       <button onClick={handleLogOut}>
       <svg
          width="40px"
          height="40px"
          viewBox="0 0 24 24"
          fill="nones"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M15 12L2 12M2 12L5.5 9M2 12L5.5 15"
            stroke="white"
            stroke-width="1.5"
            stroke-linecap="round"
            stroke-linejoin="round"
          />
          <path
            d="M9.00195 7C9.01406 4.82497 9.11051 3.64706 9.87889 2.87868C10.7576 2 12.1718 2 15.0002 2L16.0002 2C18.8286 2 20.2429 2 21.1215 2.87868C22.0002 3.75736 22.0002 5.17157 22.0002 8L22.0002 16C22.0002 18.8284 22.0002 20.2426 21.1215 21.1213C20.3531 21.8897 19.1752 21.9862 17 21.9983M9.00195 17C9.01406 19.175 9.11051 20.3529 9.87889 21.1213C10.5202 21.7626 11.4467 21.9359 13 21.9827"
            stroke="white"
            stroke-width="1.5"
            stroke-linecap="round"
          />
        </svg>
       </button>
      </div>
    </div>
  );
};

export default SideNav;
