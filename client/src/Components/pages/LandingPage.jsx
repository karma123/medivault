import React, {useEffect} from "react";
import { Link } from "react-router-dom";
import "../../App.css";
import BackgroundImage from "../../assets/images/image1.png";
import logo from "../../logo.png";
import { useNavigate } from "react-router-dom";


export default function LandingPage() {
  const loggedIn = window.localStorage.getItem("loggedIn");
  const navigate = useNavigate();

  useEffect(() => {
    if (loggedIn === "true") {
      navigate("/patientPage");
    }
  }, [loggedIn])
  

  return (
    loggedIn !== "true" && (
      <div className="relative bg-cover bg-no-repeat bg-center bg-fixed h-100%">
      <img src={BackgroundImage} alt="BackgroundImage" className="w-full h-auto" />
      <div className="absolute top-0 left-0 w-full h-full flex flex-col items-center justify-center text-white text-center">
        <div className="mb-8">
          <img src={logo} alt="Logo" className="w-40" />
        </div>
        <p>Own your health data. It is rightfully yours</p>
        <h1 className="font-normal text-xl mb-4">Welcome</h1>
        <div className="flex w-full justify-center">
          <div className="w-1/2 pr-2">
            <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded uppercase w-full">
              <Link to='/registrationChoice' className=" text-white">Sign up</Link>
            </button>
          </div>
          <div className="w-1/2 pl-2">
            <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded uppercase w-full">
              <Link to='/loginChoice' className=" text-white">Sign in</Link>
            </button>
          </div>
        </div>
      </div>
    </div>
    )
  );
}


function PatientSignUp() {
  return (
    <>
      <div className="patient-sign-up">
        <h2>Patient Sign Up</h2>
        <p>
          Medivault helps you to instantly access your records anytime,
          anywhere...
        </p>
        <button>
          <Link to="/register">Patient Sign Up</Link>
        </button>
      </div>
    </>
  );
}

function DoctorSignUp() {
  return (
    <div className="doctor-sign-up">
      <h2>Doctor Sign Up</h2>
      <p>
        Medivault pers healthcars professionals with a secure and efficient
        platform...
      </p>
      <button>
        <Link to="/register"> Doctor Sign Up </Link>
      </button>
    </div>
  );
}

// const HeaderStyle = {
//   width: "100%",
//   height: "100vh",
//   background: `url(${BackgroundImage})`,
//   backgroundPosition: "center",
//   backgroundRepeat: "no-repeat",
//   backgroundSize: "cover",
// };
