import React from "react";
import { Link } from "react-router-dom";

import "../../App.css";

export default function SignUpPage() {
  return (
    <div className=" m-5-auto">
      <div className="ml-[600px] item-center ">
        <div className="text-center">
        <h2>Join us</h2>
        <h5>Create your personal account</h5>
        </div>
        <form action="/home" className="items-center">
          <div className="">
            <label className="text-black">Username</label>
            <br />
            <input type="text" name="first_name" required />
          </div>
          <div>
            <label className="text-black">Email address</label>
            <br />
            <input type="email" name="email" required />
          </div>
          <div>
            <label className="text-black">Password</label>
            <br />
            <input type="password" name="password" required />
          </div>
          <div>
            <input
              className="text-black"
              type="checkbox"
              name="checkbox"
              id="checkbox"
              required
            />{" "}
            <span>
              I agree all statements in{" "}
              <a
                href="https://google.com"
                target="_blank"
                rel="noopener noreferrer"
              >
                terms of service
              </a>
            </span>
            .
          </div>
          <div>
            <button className="text-black" id="sub_btn" type="submit">
              Register
            </button>
          </div>
        </form>
        <footer>
          <div>
            <Link to="/">Back to Homepage</Link>.
          </div>
        </footer>
      </div>
    </div>
  );
}
