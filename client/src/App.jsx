import React from 'react'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'

import './index.css'

import LandingPage from './components/pages/LandingPage'
import LoginPage from './components/pages/LoginPage'
import SignUpPage from './components/pages/RegisterPage'
import ForgetPasswordPage from './components/pages/ForgetPasswordPage'
import RegistrationChoice from './pages/registrationChoice'
import LoginChoice from './pages/loginChoice'
import UserLogin from './pages/userLogin'
import UserRegistration from './pages/userRegistration'
import PRegister from './pages/patientRegister'
import PatientLogin from './pages/patientLogin'
import OtpEnter from './pages/otpEnter'
import PatientOtpEnter from './pages/patientOtpEnter'
import PatientDashboard from './pages/patientDashboard'
import PatientPage from './pages/patientPage'

// import './App.css'

export default function App() {
    return (
        <>
        <Router>
            <div>
                {/* <Switch> */}
                <Routes>
                    <Route exact path="/" element = {<LandingPage/>} />
                    <Route path='/loginChoice' element = {<LoginChoice/>} />
                    <Route path='/registrationChoice' element = {<RegistrationChoice/>} />
                    <Route path='/userLogin' element = {<UserLogin/>} />
                    <Route path='/userRegistration' element = {<UserRegistration/>} />
                    <Route path='/patientRegistration' element = {<PRegister/>} />
                    <Route path='/patientLogin' element = {<PatientLogin/>} />
                    <Route path='/otpEnter' element = {<OtpEnter/>} />
                    <Route path='/patientPage' element = {<PatientPage/>} />
                    <Route path='/patientOtpEnter' element = {<PatientOtpEnter/>} />
                    <Route path='/patientDashboard' element = {<PatientDashboard/>} />

                    {/* <Route path="/login" element={ <LoginPage/> } /> */}
                    {/* <Route path="/register" element={ <SignUpPage/> } /> */}
                    {/* <Route path="/forget-password" element={ <ForgetPasswordPage/> } /> */}
                    {/* <Route path="/home" component={ HomePage } /> */}
                {/* </Switch> */}
                </Routes>
               
            </div>
        </Router>
        </>
    )
}
