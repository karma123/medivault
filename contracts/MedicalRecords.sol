// // SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.0;

contract MedicalRecords {
    struct Record {
        uint256 id;
        string ipfsHash; // IPFS hash of the medical record
        address doctor;
        bool authorized;
    }

    struct Patient {
        string name;
        address[] doctors;
        mapping(uint256 => Record) records;
        uint256 recordCount;
    }

    struct Doctor {
        string name;
        address[] patients;
    }

    mapping(address => Patient) public patients;
    mapping(address => Doctor) public doctors;

    event RecordAdded(address indexed patientId, uint256 indexed recordId, string ipfsHash, address doctor);
    event AuthorizationChanged(address indexed patientId, address indexed doctor, bool authorized);

    modifier onlyPatient() {
        require(bytes(patients[msg.sender].name).length != 0, "Sender is not a registered patient");
        _;
    }

    modifier onlyDoctor() {
        require(bytes(doctors[msg.sender].name).length != 0, "Sender is not a registered doctor");
        _;
    }

    function addDoctor(string memory _name) external {
        require(bytes(doctors[msg.sender].name).length == 0, "Doctor already registered");
        doctors[msg.sender].name = _name;
    }

    function addPatientToDoctor(address _doctor, address _patient) external onlyDoctor {
        require(bytes(doctors[_doctor].name).length != 0, "Doctor not found");
        doctors[_doctor].patients.push(_patient);
    }
    

    function addPatient(string memory _name) external {
        require(bytes(patients[msg.sender].name).length == 0, "Patient already registered");
        patients[msg.sender].name = _name;
    }

    function addDoctorToPatient(address _patient, address _doctor) external onlyDoctor {
        require(bytes(patients[_patient].name).length != 0, "Patient not found");
        patients[_patient].doctors.push(_doctor);
    }

    function addRecord(address _patientId, string memory _ipfsHash) external onlyDoctor {
        require(bytes(patients[_patientId].name).length != 0, "Patient not found");
        require(isDoctorAuthorized(msg.sender, _patientId), "Doctor not authorized");

        uint256 recordId = patients[_patientId].recordCount + 1;
        patients[_patientId].records[recordId] = Record({
            id: recordId,
            ipfsHash: _ipfsHash,
            doctor: msg.sender,
            authorized: true
        });
        patients[_patientId].recordCount++;

        emit RecordAdded(_patientId, recordId, _ipfsHash, msg.sender);
    }

    function authorizeDoctor(address _doctor) external onlyPatient {
        require(bytes(doctors[_doctor].name).length != 0, "Doctor not found");
        patients[msg.sender].doctors.push(_doctor);
        doctors[_doctor].patients.push(msg.sender);
        emit AuthorizationChanged(msg.sender, _doctor, true);
    }

    function revokeAuthorization(address _doctor) external onlyPatient {
        require(bytes(patients[msg.sender].name).length != 0, "Patient not found");

        uint256 index = findDoctorIndex(patients[msg.sender].doctors, _doctor);
        require(index < patients[msg.sender].doctors.length, "Doctor not authorized");

        patients[msg.sender].doctors[index] = patients[msg.sender].doctors[patients[msg.sender].doctors.length - 1];
        patients[msg.sender].doctors.pop();

        index = findPatientIndex(doctors[_doctor].patients, msg.sender);
        doctors[_doctor].patients[index] = doctors[_doctor].patients[doctors[_doctor].patients.length - 1];
        doctors[_doctor].patients.pop();

        emit AuthorizationChanged(msg.sender, _doctor, false);
    }

    function getPatientRecords(address _patientId) external view onlyPatient returns (Record[] memory) {
        Record[] memory patientRecords = new Record[](patients[_patientId].recordCount);
        for (uint256 i = 0; i < patients[_patientId].recordCount; i++) {
            patientRecords[i] = patients[_patientId].records[i + 1];
        }
        return patientRecords;
    }

    function isDoctorAuthorized(address _doctor, address _patient) internal view returns (bool) {
        for (uint256 i = 0; i < patients[_patient].doctors.length; i++) {
            if (patients[_patient].doctors[i] == _doctor) {
                return true;
            }
        }
        return false;
    }

    function findDoctorIndex(address[] storage _array, address _doctor) internal view returns (uint256) {
        for (uint256 i = 0; i < _array.length; i++) {
            if (_array[i] == _doctor) {
                return i;
            }
        }
        return _array.length;
    }

    function findPatientIndex(address[] storage _array, address _patient) internal view returns (uint256) {
        for (uint256 i = 0; i < _array.length; i++) {
            if (_array[i] == _patient) {
                return i;
            }
        }
        return _array.length;
    }
}
