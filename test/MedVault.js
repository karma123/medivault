const { assert, expect } = require("chai");
const { ethers } = require("hardhat");

describe("MedicalRecords Contract", function () {
  let MedicalRecords;
  let medicalRecords;
  let patient;
  let doctor;
  const IPFS_HASH = "QmXyZaBcDeFgHiJkLmNoPqRsTuVwXyZaBcDeFgHiJkLmNoPqRsTuVw";

  beforeEach(async function () {
    MedicalRecords = await ethers.getContractFactory("MedicalRecords");
    [patient, doctor] = await ethers.getSigners();
    medicalRecords = await MedicalRecords.deploy();
  });

  describe("Add Medical Record", function () {
    it("should add a new medical record", async function () {
      // Call the addMedicalRecord function
      await medicalRecords.addMedicalRecord(patient.address, IPFS_HASH);
      // Check if the RecordAdded event is emitted with the correct arguments
      await expect(medicalRecords.addMedicalRecord(patient.address, IPFS_HASH))
        .to.emit(medicalRecords, "RecordAdded")
        .withArgs(patient.address, patient.address, IPFS_HASH);
    });

    it("should update an existing medical record", async function () {
      await medicalRecords.addMedicalRecord(patient.address, IPFS_HASH);
      const updatedIPFSHash =
        "AbCdEfGhIjKlMnOpQrStUvWxYzAbCdEfGhIjKlMnOpQrStUvWxYz";
      await expect(
        await medicalRecords.addMedicalRecord(patient.address, updatedIPFSHash)
      )
        .to.emit(medicalRecords, "RecordAdded")
        .withArgs(patient.address, patient.address, updatedIPFSHash);
    });

    it("should revert if caller is not the patient or authorized doctor", async function () {
      const unauthorizedDoctor = await ethers.getSigner();
      await expect(
        medicalRecords
          .connect(doctor)
          .addMedicalRecord(patient.address, IPFS_HASH)
      ).to.be.revertedWith("Unauthorized access");
    });

    it("should revert if patient address is invalid", async function () {
      await expect(
        medicalRecords.addMedicalRecord(ethers.constants.AddressZero, IPFS_HASH)
      ).to.be.revertedWith("Unauthorized access");
    });
  });

  describe("Get Medical Record", function () {
    it("should return the correct medical record", async function () {
      await medicalRecords.addMedicalRecord(patient.address, IPFS_HASH);
      const medicalRecord = await medicalRecords.getMedicalRecord(
        patient.address
      );
      expect(medicalRecord, "Returned medical record is incorrect").to.equal(
        IPFS_HASH
      );
    });

    it("should revert if medical record does not exist for the patient", async function () {
      await expect(
        medicalRecords.getMedicalRecord(patient.address),
        "Retrieving medical record for non-existent patient should revert"
      ).to.be.revertedWith("Medical record does not exist for the patient");
    });

    it("should revert if caller is not the patient or authorized doctor", async function () {
      const unauthorizedDoctor = await ethers.getSigner();
      await expect(
        medicalRecords
          .connect(unauthorizedDoctor)
          .getMedicalRecord(patient.address)
      ).to.be.revertedWith("Medical record does not exist for the patient");
    });
  });

  describe("Authorize Doctor", function () {
    it("should authorize a doctor to access patient's medical records", async function () {
      await expect(medicalRecords.authorizeDoctor(doctor.address)).to.not.be
        .reverted;
    });

    it("should revert if doctor address is invalid", async function () {
      await expect(
        medicalRecords.authorizeDoctor(ethers.constants.AddressZero),
        "Invalid doctor address"
      ).to.be.revertedWith("Invalid doctor address");
    });

    it("should revert if doctor is already authorized", async function () {
      await medicalRecords.authorizeDoctor(doctor.address);
      await expect(
        medicalRecords.authorizeDoctor(doctor.address)
      ).to.be.revertedWith("Doctor already authorized");
    });
  });

  describe("Revoke Authorization", function () {
    it("should revoke authorization of a doctor to access patient's medical records", async function () {
      await medicalRecords.authorizeDoctor(doctor.address);
      await expect(medicalRecords.revokeAuthorization(doctor.address)).to.not.be
        .reverted;
    });

    it("should revert if doctor address is invalid", async function () {
      await expect(
        medicalRecords.revokeAuthorization(ethers.constants.AddressZero),
        "Invalid doctor address"
      ).to.be.revertedWith("Invalid doctor address");
    });
    it("should revert if doctor is not authorized", async function () {
      await expect(
        medicalRecords.revokeAuthorization(doctor.address),
        "Revoking authorization of non-authorized doctor should revert"
      ).to.be.revertedWith("Doctor not authorized");
    });
  });
});